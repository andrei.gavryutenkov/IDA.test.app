//
//  DetailScreenViewController.swift
//  Ainstags
//
//  Created by Andrey Gavryutenkov on 7/4/19.
//  Copyright © 2019 Andrey Gavryutenkov. All rights reserved.
//

import Foundation
class DetailScreenViewController: BaseViewController, BaseViewProtocol, BaseViewControllerOutputProtocol {
    
    typealias ViewClass = DetailScreenView
    typealias OutputClass = DetailScreenViewOutput
    
    

}


extension DetailScreenViewController: DetailScreenViewInput {
}
